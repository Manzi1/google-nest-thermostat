#!/usr/bin/python3

# This script has been created using excerpts from:
# https://www.wouternieuwerth.nl/controlling-a-google-nest-thermostat-with-python/

import os
from time import sleep
from math import trunc
import requests
import time
os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

# Red 17 3v
# Purple 8 Grnd
# Green  GPIO 4
temp_sensor_1 = '/sys/bus/w1/devices/28-011458674aaa/w1_slave'
timestamp = time.strftime("%Y-%m-%d %H:%M:%S")
device_0_name = 'enterprises/78df6845-7cdd-4c4e-985b-XXXXXXXXXXXX/devices/AVPHwEt-Ct4sxA9BDULR6Q7vFo-08gO_TaknhP6AC-7tK3f5hS_b1Bg1E_Sz-HmYL4YFrOeSloZR5XMRllbyq5zTnTl-OA'
url_set_mode = 'https://smartdevicemanagement.googleapis.com/v1/' + device_0_name + ':executeCommand'
client_id = 'XXXXXXXXXX-4h6mk5r93athigh96cfnheobql9la4io.apps.googleusercontent.com'
client_secret = 'W0HkibH98DkicHhXXXXXXXXXX'
refresh_token = '1//03nFTwi9Q7Y5iCgYIARAAGAMSNwF-L9IrYQahCJksg2Qr1vC9pWY2e-9uZg8feOiNLVb0ds1JQ6SXHkwiqB7RYAaXXXXXXXXXXXX'
params = (
    ('client_id', client_id),
    ('client_secret', client_secret),
    ('refresh_token', refresh_token),
    ('grant_type', 'refresh_token'),
)
# Refresh NEST token
response = requests.post('https://www.googleapis.com/oauth2/v4/token', params=params)
response_json = response.json()
access_token = response_json['token_type'] + ' ' + response_json['access_token']

def read_temp_raw(temp_sensor):
    f = open(temp_sensor, 'r')
    lines = f.readlines()
    f.close()
    return lines

def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return trunc(stepper * number) / stepper

def read_temp(temp_sensor):
    lines = read_temp_raw(temp_sensor)
    while lines[0].strip()[-3:] != 'YES':
        sleep(0.2)
        lines = read_temp_raw(temp_sensor)
    temp_result = lines[1].find('t=')
    if temp_result != -1:
        temp_string = lines[1][temp_result + 2:]
        temp = truncate((float(temp_string) / 1000.0),1)
        return temp

print("Xanders room is at = ", read_temp(temp_sensor_1))
if read_temp(temp_sensor_1) > 17.9:
    print(timestamp,"Its a bit hot setting temp to 18")
    set_temp_to = 18.0
else:
    print(timestamp,"Its a bit cold setting temp to 23")
    set_temp_to = 23.0

# Set headers and send command to NEST
headers = {
    'Content-Type': 'application/json',
    'Authorization': access_token,
}
data = '{"command" : "sdm.devices.commands.ThermostatTemperatureSetpoint.SetHeat", "params" : {"heatCelsius" : ' + str(set_temp_to) + '} }'
response = requests.post(url_set_mode, headers=headers, data=data)

# Add a stamp in the log
#with open("temp.log", "a") as text_file:
#    print(timestamp,"Temp set to: {}".format(set_temp_to), file=text_file)
